
public class Module {
	private String moduleCode;
	private Occurence modoccurence;
	
	
	public Module(String string, Occurence occurence) {
		moduleCode = string;
		modoccurence = occurence;
	}
	
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public Occurence getModoccurence() {
		return modoccurence;
	}
	public void setModoccurence(Occurence modoccurence) {
		this.modoccurence = modoccurence;
	}
	
	public String display() {
		return moduleCode + " | " + modoccurence.display();
	}
	
	
}
