
public class Occurence {
	private String code;
	private String day;
	private String startTime;

	
	public Occurence(String string, String string2, String string3) {
		code = string;
		day = string2;
		startTime = string3;
		
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
		
	public String display() {
		return("Occurence: " + getCode() + " | " + "Day: " + getDay() + " | " + getStartTime());
	}
	
	
	
	
}
